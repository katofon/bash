class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string :body, null: false, default: ""
      t.datetime :created_at, null: false
      t.integer :rating, null: false, default: 0
    end

    add_index :articles, :created_at
    add_index :articles, :rating
  end
end
