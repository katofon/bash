Rails.application.routes.draw do
  root to: 'articles#index'

  resources :articles, only: [:new, :create, :show] do
    get :like, on: :member
    get :dislike, on: :member
  end
end
